# Flight Planner

This Angular app (version 8.1.3) is a flight planner for drones.

## Development server

To develop locally run the development server following one of these recipes:

```
yarn install --frozen-lockfile
yarn start
```

## Log

### 14 January

- 7:30-8pm: initial setup
- 8-8:30pm: initial setup of the agm map

### 15 January

- 12-12:30pm: dealt with issue with hotreloading (not detecting changes in scss)
- 2:00-3:00pm: added polylines
- 3:00-6:00pm: added a bit of material layout. I experienced a lot of problems
  with the hot reloading of the app. I should break the component into
  subcomponents and add a service to manage the flight plans.
- 7-7:45pm: fixing problems with saving, clearing and deleting plans.

### 16 January

- 7:30-8:30am: making all more modular with smaller components and service.

## Want to help?

Want to file a bug, contribute some code, or improve documentation? Excellent!
Read up on our guidelines for [contributing][contributing].

[contributing]: https://gitlab.com/esanzgar/flight-planner-8/blob/master/CONTRIBUTING.md
