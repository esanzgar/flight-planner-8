import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RootComponent } from './containers/root/root.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgmCoreModule } from '@agm/core';
import { MaterialModule } from './material.module';

import { PlansService } from './services/plans.service';

import { GmapComponent } from './components/gmap/gmap.component';
import { ListPlansComponent } from './components/list-plans/list-plans.component';
import { HeaderComponent } from './components/header/header.component';
import { ControlsComponent } from './components/controls/controls.component';

@NgModule({
  declarations: [
    RootComponent,
    GmapComponent,
    ListPlansComponent,
    HeaderComponent,
    ControlsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB2HefEdfnaqrGl17vnI6JmltWFOF3HCgg'
    }),
    MaterialModule
  ],
  providers: [PlansService],
  bootstrap: [RootComponent]
})
export class AppModule {}
