import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from '@angular/core';

@Component({
  selector: 'fp-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlsComponent {
  @Output()
  clear = new EventEmitter();

  @Output()
  save = new EventEmitter();

  clearPath() {
    this.clear.emit();
  }

  savePath() {
    this.save.emit();
  }
}
