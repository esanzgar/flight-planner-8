import {
  Component,
  EventEmitter,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Output
} from '@angular/core';

import { MouseEvent } from '@agm/core';
import { Coordinate } from '../../../models';

@Component({
  selector: 'fp-gmap',
  templateUrl: './gmap.component.html',
  styleUrls: ['./gmap.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GmapComponent implements OnInit {
  lat = 46.5294895;
  lng = 6.6006589;
  zoom = 20;

  @Input()
  path: Coordinate[] = [];

  @Output()
  mapClick = new EventEmitter<Coordinate>();

  ngOnInit() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
      });
    }
  }

  click(event: MouseEvent) {
    this.mapClick.emit(event.coords);
  }
}
