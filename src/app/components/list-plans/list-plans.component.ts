import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from '@angular/core';

import { Plan } from '../../../models';

@Component({
  selector: 'fp-list-plans',
  templateUrl: './list-plans.component.html',
  styleUrls: ['./list-plans.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListPlansComponent {
  @Input()
  plans: Plan[] = [];

  @Output()
  display = new EventEmitter<number>();

  @Output()
  remove = new EventEmitter<number>();

  loadPlan(index: number) {
    this.display.emit(index);
  }

  deletePlan(index: number) {
    this.remove.emit(index);
  }
}
