import { Component, OnInit } from '@angular/core';

import { PlansService } from '../../services/plans.service';
import { Coordinate, Plan } from '../../../models';

@Component({
  selector: 'fp-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})
export class RootComponent implements OnInit {
  path: Coordinate[] = [];
  plans: Plan[] = [];
  planIndex = 0;

  constructor(private plansService: PlansService) {}

  ngOnInit() {
    this.plans = this.plansService.fetch();
    this.planIndex = this.plans.length;
  }

  click(event: Coordinate) {
    this.path = [...this.path, event];
  }

  savePath() {
    if (this.path.length > 1) {
      this.plans[this.planIndex] = this.path;
      this.plans = [...this.plans];
      this.planIndex = this.plans.length;
      this.plansService.save(this.plans);
    }

    this.clearPath();
  }

  clearPath() {
    this.path = [];
  }

  loadPlan(index: number) {
    this.planIndex = index;
    this.path = this.plans[index];
  }

  deletePlan(index: number) {
    if (index === this.planIndex) {
      this.path = [];
    }
    this.plans.splice(index, 1);
    this.plans = [...this.plans];
    this.planIndex = this.plans.length;
    this.plansService.save(this.plans);
  }
}
