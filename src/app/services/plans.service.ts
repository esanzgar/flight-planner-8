import { Injectable } from '@angular/core';

import { Plan } from '../../models';

const STORAGE_KEY = 'fp-plans';

@Injectable({
  providedIn: 'root'
})
export class PlansService {
  fetch(): Plan[] {
    const storedPlans = window.localStorage.getItem(STORAGE_KEY) || '[]';
    return JSON.parse(storedPlans);
  }

  save(plans: Plan[]) {
    window.localStorage.setItem(STORAGE_KEY, JSON.stringify(plans));
  }
}
