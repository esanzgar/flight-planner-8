export interface Coordinate {
  lat: number;
  lng: number;
}

export type Plan = Coordinate[];
